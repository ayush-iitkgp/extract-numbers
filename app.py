import os
import traceback
import json
from optparse import OptionParser
from slots import Slots, ExtractSlots

def main(aname):
    pass

def test_currency(aname):
    slotsObj = ExtractSlots(aname)
    fullFName = os.path.join("./testData", 'currency.txt')
    t_o = []
    with open(fullFName, "r", encoding = "utf8") as f:
        for line in f:
            print(line.strip("\n"))
            d = dict()
            d['sentence'] = line.strip("\n")
            moneyObjs = slotsObj._extractMoney(line.strip("\n"))
            d['output'] = moneyObjs
            # print(moneyObjs)
            t_o.append(d)
    with open("./tests/currency.json", "w", encoding="utf8") as f:
        json.dump(t_o, f, indent=4)
                    
def test_date(aname):
    slotsObj = ExtractSlots(aname)
    # t_o = []
    fullFName = os.path.join("./testData", 'date.txt')
    with open(fullFName, "r", encoding = "utf8") as f:
        for line in f:
            print(line.strip("\n"))
            d = dict()
            d['sentence'] = line.strip("\n")
            matching_date = slotsObj._extractDateAndTime(line.strip("\n"))
            d['output'] = matching_date
            # t_o.append(d)
    # with open("./tests/date.json", "w", encoding="utf8") as f:
    #     json.dump(t_o, f, indent=4)

def test_percentage(aname):
    slotsObj = ExtractSlots(aname)
    t_o = []
    fullFName = os.path.join("./testData", 'percentage.txt')
    with open(fullFName, "r", encoding = "utf8") as f:
        for line in f:
            print(line.strip("\n"))
            d = dict()
            d['sentence'] = line.strip("\n")
            percentages = slotsObj._extractPercentage(line.strip("\n"))
            d['output'] = percentages
            t_o.append(d)
            print(percentages)
    with open("./tests/percentage.json", "w", encoding="utf8") as f:
        json.dump(t_o, f, indent=4)

def test_all(aname):
    slotsObj = ExtractSlots(aname)
    fullFName = os.path.join("./testData", 'all.txt')
    t_o = []
    with open(fullFName, "r", encoding = "utf8") as f:
        for line in f:
            print(line.strip("\n"))
            allObj = slotsObj.extractEntities(line.strip("\n"))
            t_o.append(allObj)
    with open("./tests/all.json", "w", encoding="utf8") as f:
        json.dump(t_o, f, indent=4)

if __name__ == "__main__":
    parser = OptionParser(usage="Usage: %prog mode [options]")
    # parser.add_option("-e", "--eventtypes", dest="eventTypes", action="store", type="string", default = "./eventTypeSlots.json", help = "Path to the settings file")
    parser.add_option("-a", "--abbreviations", dest="abbreviations", action="store", type="string", default = "./abbreviations.json", help = "Path to the abbreviation file")
    
    options, args = parser.parse_args()

    # assert os.path.exists(options.eventTypes), "File %s does not exist. Unable to load the settings. Exiting" % (options.eventTypes)
    assert os.path.exists(options.abbreviations), "File %s does not exist. Unable to load the settings. Exiting" % (options.abbreviations)

    main(options.abbreviations)

    # test_currency(options.abbreviations)
    
    # test_date(options.abbreviations)

    # test_percentage(options.abbreviations)

    test_all(options.abbreviations)