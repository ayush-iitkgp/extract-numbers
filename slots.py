import os
import traceback
import json
import re
from datetime import datetime

# import flair
# import torch
# from flair.data import Sentence
# from flair.models import SequenceTagger
# from money_parser import price_str
from price_parser.src import Price
from dateparser.search import search_dates

ONE = 1

def loadJsonFile(fname):
        assert os.path.exists(fname)
        try:
            with open(fname, "r", encoding="utf8") as f:
                data = json.load(f)
                return data
        except Exception as ex:
            traceback.print_exc()
            return {}

class Slots:
    def __init__(self, type_, text, startCh, endCh, number = None, currency = None, date = None, time = None):
        self.type = type_
        self.text = text
        self.startCh = startCh
        self.endCh = endCh
        self.number = number
        self.currency = currency
        self.date = date
        self.time = time

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, 
            sort_keys=True, indent=4)

class ExtractSlots:
    def __init__(self, aname):
        # self.data = loadJsonFile(ename)
        self.abbreviations = loadJsonFile(aname)
        # self.model = self.loadFlairModel('ner-ontonotes')
    
    def extractEntities(self, text):
        output = []
        moneyObjs = self._extractMoney(text)
        dateObjs = self._extractDateAndTime(text)
        percentObjs = self._extractPercentage(text)
        output = output + moneyObjs + dateObjs + percentObjs
        return {"sentence": text, "output": output}

    def _extractMoney(self, text):
        moneyObjs = []
        price = Price.fromstring(text, decimal_separator=".")
        if price.currency == None:
            return []
        number, exact_text = self._normalizeMoneyAndExtractExactText(price, text)
        startCh, endCh = self._extractIndex(exact_text, text)
        moneyObj = Slots('money', exact_text, startCh, endCh, number = number, currency = price.currency)
        moneyObjs.append(moneyObj.__dict__)
        return moneyObjs

    def _extractExactText(self):
        return ""
        # pass

    def _extractIndex(self, exact_text, text):
        startCh = text.find(exact_text)
        return startCh, startCh + len(exact_text)

    def _normalizeMoneyAndExtractExactText(self, price, text):
        value, exact_text = self._findValue(price.amount_text, text)
        return value*price.amount_float, exact_text

    def _findValue(self, amount, text):
        tokens = text.split(" ")
        abbreviation = None
        exact_substring = None
        index = 0
        for token in tokens:
            if amount in token:
                break
            index = index + 1 
                
        matched_token = tokens[index].lower()
        exact_substring = tokens[index]
        if "m" in matched_token:
            abbreviation = "M"
        if "b" in matched_token.lower():
            abbreviation = "B"
        if "-" in matched_token:
            abbreviation = token.split("-")[1]

        stripped_abbreviation = abbreviation
        if abbreviation is None:
            abbreviation = tokens[index + 1]
            characters_to_remove = ".,:;"
            pattern = "[" + characters_to_remove + "]"
            stripped_abbreviation = re.sub(pattern, "", abbreviation)
            exact_substring = exact_substring + " " + stripped_abbreviation

        # characters_to_remove = ".,:;"
        # pattern = "[" + characters_to_remove + "]"
        # stripped_abbreviation = re.sub(pattern, "", abbreviation)
        # exact_substring = exact_substring + " " + stripped_abbreviation
        for ab in self.abbreviations:
            if stripped_abbreviation in ab['abbreviations']:
                return ab['value'], exact_substring
        return ONE, exact_substring            

    def _extractDateAndTime(self, text):
        dateAndTimes = search_dates(text)
        # print(dateAndTime)
        dateObjs = []
        for d in dateAndTimes:
            date_str = d[1].strftime("%d-%m-%Y")
            exact_text = d[0]
            startCh, endCh = self._extractIndex(exact_text, text)
            dateObj = Slots('date', exact_text, startCh, endCh, date = date_str)
            dateObjs.append(dateObj.__dict__)
        return dateObjs

    def _extractPercentage(self, text):
        regex = r"(\d+(\.\d+)?%)|(\d+(\.\d+)?\s\bpercent)"
        matches = re.finditer(regex, text, re.IGNORECASE)
        percentageObjs = []
        for matchNum, match in enumerate(matches, start=1):
            exact_text = match.group()
            startCh = match.start()
            endCh = match.end()
            # print ("Match {matchNum} was found at {start}-{end}: {match}".format(matchNum = matchNum, start = match.start(), end = match.end(), match = match.group()))
            # for groupNum in range(0, len(match.groups())):
            #     groupNum = groupNum + 1
            #     print ("Group {groupNum} found at {start}-{end}: {group}".format(groupNum = groupNum, start = match.start(groupNum), end = match.end(groupNum), group = match.group(groupNum)))
            if 'percent' in exact_text.lower():
                number = float(exact_text.split()[0])
            else:
                number = float(exact_text[:-1])/100
            percentageObj = Slots('percentage', exact_text, startCh, endCh, number=number)
            percentageObjs.append(percentageObj.__dict__)
        return percentageObjs

    # def loadFlairModel(self, modelName):
    #     if "fast" in modelName:
    #         flair.device = torch.device('cpu')
    #     flairModel = SequenceTagger.load(modelName)
    #     return flairModel